# X-Service Moodle Starter

Dieses Repository dient dazu, eine neue Moodle X-Service Installation zu kopieren. Dafür wird eine Unix Konsole (z.B. git bash) benötigt.
 
```bash
$ git clone git@bitbucket.org:deutschewindtechnik/moodle-x-service-starter.git moodle
$ cd moodle
$ ./git.sh
```